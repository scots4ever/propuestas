---
layout: 2020/post
section: propuestas
category: devrooms
title: [Tecnología para el fomento de la lengua andaluza]
---

En [AndaluGeeks](https://andaluh.es) nos gustaría contar con un espacio dentro del evento eslibre. Debido a la carga política en cuanto a derechos digitales que hay detrás del evento, nos sentimos muy ilusionados de poder crear comunidad en torno al evento.

## Comunidad o grupo que lo propone

[AndaluGeeks](https://andaluh.es) somos un grupo de profesionales de la informática, la programación, el diseño gráfico y las TIC que creamos, desarrollamos y gestionamos proyectos tecnológicos de codigo libre y abierto alrededor de la lengua, la educación y la difusión de la cultura andaluza.

Tras dos años de recorrido, **AndaluGeeks lo componemos 40 personas**, 12 de las cuales contribuyen con código a nuestras librerías de desarrollo y aplicaciones. Visita [nuestro espacio en GitHub](https://github.com/andalugeeks) para conocer más. Nuestras apps más significativas son:

* **Andaluh SDK**: Una librería de transcripción castellano a andaluz escrito. Disponible para [python](https://github.com/andalugeeks/andaluh-py), [javascript](https://github.com/andalugeeks/andaluh-js), [java](https://github.com/andalugeeks/andaluh-java), [.NET](https://github.com/andalugeeks/andaluh-net), [rust](https://github.com/andalugeeks/andaluh-rs) y [API REST](https://api.andaluh.es/docs)
* **Transcriptor Andaluh**: Una aplicación [web](https://andaluh.es/transcriptor/) y [móvil](https://play.google.com/store/apps/details?id=es.andaluh.transcriptor&showAllReviews=true) para transcribir castellano a andaluz basada en [andaluh-cordova](https://github.com/andalugeeks/andaluh-cordova) (phonegap).
* **Teclado Andaluh**: Un [teclado #opensource para escribir diréctamente en ortografía andaluza](https://andaluh.es/teclado-andaluz), con autocorrecciones, sugerencias y texto predictivo. [Disponible para Android](https://play.google.com/store/apps/details?id=com.anysoftkeyboard.languagepack.andalusia).
* **Bots para apps de mensajería**: Disponible para [Telegram](https://github.com/andalugeeks/andaluh-telegram), [Slack](https://github.com/andalugeeks/andaluh-slack) y [Discord](https://github.com/andalugeeks/andaluh-discord).
* **Minecraft Andaluz**: Mojang, la empresa detrás de Minecraft, aceptó [nuestra traducción a Andaluz](https://andaluh.es/minecraft-andaluz) y hoy está disponible de forma oficial. 

Como comunidad abierta de desarrolladoras andaluzas queremos contribuir en nuestra tierra al establecimiento y la potenciación de redes profesionales en el campo tecnológico. Si te quieres unir a nuestra comunidad: [¡te esperamos en slack!](https://join.slack.com/t/andalugeeks/shared_invite/enQtNjE1NjQyNjU0ODg2LTQxYmYzYWM5MWI1YWMwMzBjMGM0NmQ0MDk2YjU0YzdjNzc1YWM4YTNjMmZlNGJjYTYyY2I4NmJlYzMzNzg3MTA)

### Contactos

* Sergio Soto Núñez: sergio.soto.nunez at gmail dot com
* J. Félix Ontañón: felixonta at gmail dot com

## Público objetivo

Se trata de un espacio abierto a todas las personas (técnicas o no) que sientan curiosidad por el andaluz escrito. Muy especialmente si tienen interés en los siguientes temas:

* **La internacionalización (i18n) y localización (i10n) de aplicaciones**. Es algo que muchas veces damos por obvio. El ejercicio en AndaluGeeks muestra las dificultades y retos a la hora de incorporar un nuevo item de i18n e i10n.
* **Inteligencia Artificial**. Hemos comenzado a desarrollar la transcripción inversa andaluh a castellano y vamos a utilizar técnicas avanzadas de machine-learning y redes neuronales para ello.
* **DevOps**. En AndaluGeeks autogestionamos nuestra infraestructura de servicios con docker. Queremos incorporar mecanismos de integración continua / despliegue continuo (CICD) y aplicar una capa de API Management a nuestro [API REST](https://api.andaluh.es/docs).

## Tiempo

Medio día.

## Día

Indiferente.

## Formato

La actividad estará organizada en varios tracks, todos consecutivos:

1. Bienvenida a la comunidad, espacio dedicado a conocer el objetivo de esta comunidad. Será un espacio abierto a personas tanto técnicas como de otros ambitos (30 minutos).
2. Andalugeeks: hablaremos sobre los [proyectos en marcha](https://andaluh.es/es/proyectos/) y algunas novedades (30 minutos).
3. Composición de mesas de trabajo. Hemos definido algunas de ellas:
 + Andaluh SDK, para trabajar en los diferes módulos y librerías de traducción organizados por lenguajes.
 + ML y transcripción inversa. Grupo de trabajo dedicado a machine learning.
 + Iniciación de nuevas personas colaboradoras. Este espacio estará dedicado a dar cabida a personas que tienen interés en nuestros proyectos o a ayudar con el mantenimiento de nuestra infraestructura cloud.
4. Sesión Plenaria de la comunidad y cierre.

## Comentarios


## Condiciones

* [x] &nbsp;Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
* [x] &nbsp;Al menos una persona entre los que proponen la devroom estará presente el día agendado para la *devroom*.
* [x] &nbsp;Acepto coordinarme con la organización de esLibre.
* [x] &nbsp;Entiendo que si no hay un programa terminado para la fecha que establezca la organización, la *devroom* podría retirarse.
